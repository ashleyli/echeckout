import Promotion.Promotion;
import Promotion.SingleProductDeal;
import Promotion.MultiProductDeal;
import Store.Inventory;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class StoreTest {

    @Test
    public void testProductModifications() {
        Assert.assertEquals("Expect zero inventory before creating products",
                0, Inventory.getProductMap().size());

        Inventory.createNewProduct("Apple", 2.5);
        Inventory.createNewProduct("Orange", 1.5);
        Assert.assertEquals("Expect created two products",
                2, Inventory.getProductMap().size());
        Assert.assertEquals("Expect created apple of price 2.5",
                "1|Apple|2.5", Inventory.getProduct(1).toString());
        Assert.assertEquals("Expect created orange of price 1.5",
                "2|Orange|1.5", Inventory.getProduct(2).toString());

        Inventory.amendProductDescription(1, "Apple V2");
        Assert.assertEquals("Expect amended product 1 description to Apple V2",
                "1|Apple V2|2.5", Inventory.getProduct(1).toString());

        Inventory.amendProductUnitPrice(2, 2);
        Assert.assertEquals("Expect amended product 2 unit price to 2.0",
                "2|Orange|2.0", Inventory.getProduct(2).toString());

        Inventory.removeProduct(1);
        Assert.assertEquals("Expect inventory is zero after removing product",
                1, Inventory.getProductMap().size());
        Assert.assertEquals("Expect product 1 is deleted.", null, Inventory.getInventory(1));

        Inventory.addProductQty(2, 20);
        Assert.assertEquals("Expect product 2 has qty 20.", 20, Inventory.getInventory(2).intValue());

        Inventory.reduceProductQty(2,30);
        Assert.assertEquals("Expect qty of product 2 unchanged.", 20, Inventory.getInventory(2).intValue());
        Inventory.reduceProductQty(2,10);
        Assert.assertEquals("Expect qty of product 2 reduced to 10.", 10, Inventory.getInventory(2).intValue());
    }

    @Test
    public void testPromotionDeals() {
        Assert.assertEquals("Expected no product deal for product 1",
                null, Promotion.getSingleProductDeal(1));

        SingleProductDeal singleProductDeal = new SingleProductDeal(1, 2, 0.75);
        Promotion.addSingleProductDeal(singleProductDeal);
        Assert.assertEquals("Expect created single product deal for product 1",
                "productId:1|minimalQty:2|discountPercent:0.75",
                Promotion.getSingleProductDeal(1).toString());

        Assert.assertEquals("Expected no product deal for product 2",
                null, Promotion.getMultiProductDeal(2));

        HashMap qualifiedQty = new HashMap();
        qualifiedQty.put(2, 1);
        qualifiedQty.put(3, 1);
        HashMap discountMap = new HashMap();
        discountMap.put(2, 1);
        discountMap.put(3, 0.5);
        MultiProductDeal multiProductDeal = new MultiProductDeal(qualifiedQty, discountMap);
        Promotion.addMultiProductDeal(multiProductDeal);
        Assert.assertEquals("Expect created multi product deal for product 2",
                "ProductId|Eligible Qty: 2|1;3|1;ProductId|Discount Percent: 2|1;3|0.5;",
                Promotion.getMultiProductDeal(2).toString());
        Assert.assertEquals("Expect created multi product deal for product 3",
                "ProductId|Eligible Qty: 2|1;3|1;ProductId|Discount Percent: 2|1;3|0.5;",
                Promotion.getMultiProductDeal(3).toString());
    }
}
