import Checkout.Checkout;
import Promotion.MultiProductDeal;
import Promotion.Promotion;
import Promotion.SingleProductDeal;
import Store.Inventory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.logging.Logger;

public class CheckoutTest {
    public static final Logger LOGGER = Logger.getLogger(CheckoutTest.class.getName());

    @Before
    public void Setup() {
        Inventory.createNewProduct("Apple", 2);
        Inventory.addProductQty(1, 10);
        Inventory.createNewProduct("Laptop", 1000);
        Inventory.addProductQty(2, 5);
        Inventory.createNewProduct("Mouse", 100);
        Inventory.addProductQty(3, 3);
        LOGGER.info("Set up inventory data before running test.");

        SingleProductDeal appleDeal = new SingleProductDeal(1, 2, 0.25);
        HashMap qualifiedQty = new HashMap();
        qualifiedQty.put(2, 1);
        qualifiedQty.put(3, 1);
        HashMap discountMap = new HashMap();
        discountMap.put(2, 0.0);
        discountMap.put(3, 1.0);
        MultiProductDeal laptopDeal = new MultiProductDeal(qualifiedQty, discountMap);
        Promotion.addSingleProductDeal(appleDeal);
        Promotion.addMultiProductDeal(laptopDeal);
        LOGGER.info("Set up promotion deals before running test.");
    }

    @After
    public void TearDown() {
        Inventory.getProductMap().clear();
        Inventory.getProductInventory().clear();
        Promotion.getMultiProductDeal().clear();
        Promotion.getSingleProductDeal().clear();
    }

    @Test
    public void testBasketModification() {
        Checkout checkout = new Checkout();
        Assert.assertEquals("Expect shopping basket is empty before adding anything",
                0, checkout.getShoppingBasket().size());

        checkout.addProduct(1, 3);
        Assert.assertEquals("Expect added three apple",
                3, checkout.getShoppingBasket().get(1).intValue());
        Assert.assertEquals("Expect apple inventory reduced to seven",
                7, Inventory.getInventory(1).intValue());

        checkout.addProduct(2, 1);
        Assert.assertEquals("Expect added one laptop",
                1, checkout.getShoppingBasket().get(2).intValue());
        Assert.assertEquals("Expect laptop inventory reduced to four",
                4, Inventory.getInventory(2).intValue());

        checkout.addProduct(3, 1);
        Assert.assertEquals("Expect added one mouse",
                1, checkout.getShoppingBasket().get(3).intValue());
        Assert.assertEquals("Expect mouse inventory reduced to two",
                2, Inventory.getInventory(3).intValue());

        checkout.addProduct(4, 1);
        Assert.assertEquals("Expect cant add product 4 as no inventory",
                null, checkout.getShoppingBasket().get(4));

        Assert.assertEquals("Expect added three products",
                3, checkout.getShoppingBasket().size());

        checkout.amendQty(3, 4);
        Assert.assertEquals("Expect qty unchanged as not enough stock",
                1, checkout.getShoppingBasket().get(3).intValue());
        Assert.assertEquals("Expect inventory qty unchanged",
                2, Inventory.getInventory(3).intValue());

        checkout.amendQty(3, 2);
        Assert.assertEquals("Expect mouse qty changed to two",
                2, checkout.getShoppingBasket().get(3).intValue());
        Assert.assertEquals("Expect inventory qty changed to one",
                1, Inventory.getInventory(3).intValue());

        checkout.removeProduct(1);
        Assert.assertEquals("Expect apple qty changed to zero",
                null, checkout.getShoppingBasket().get(1));
        Assert.assertEquals("Expect inventory qty changed to ten",
                10, Inventory.getInventory(1).intValue());
    }

    @Test
    public void testTotalPrice() {
        Checkout checkout = new Checkout();

        checkout.addProduct(1, 3);
        checkout.addProduct(2, 1);
        checkout.addProduct(3, 1);
        Assert.assertEquals("Expect total price after discount is 1005",
                1005, checkout.getTotalPrice(), 0.0);

        checkout.amendQty(1, 2);
        Assert.assertEquals("Expect total price after discount is 1003",
                1003, checkout.getTotalPrice(), 0.0);

        checkout.amendQty(3, 2);
        Assert.assertEquals("Expect total price after discount is 1103",
                1103, checkout.getTotalPrice(), 0.0);

        checkout.amendQty(2, 0);
        Assert.assertEquals("Expect total price after discount is 203",
                203, checkout.getTotalPrice(), 0.0);

        checkout.amendQty(1, 13);
        Assert.assertEquals("Expect total price after discount is unchanged",
                203, checkout.getTotalPrice(), 0.0);
    }
}
