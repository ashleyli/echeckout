package Promotion;

import Store.Inventory;

import java.util.*;
import java.util.logging.Logger;

public class Promotion {
    public static final Logger LOGGER = Logger.getLogger(Promotion.class.getName());
    private static Map<Integer, SingleProductDeal> singleProductDeals = new HashMap<>();
    private static Map<Integer, MultiProductDeal> multiProductDeals = new HashMap<>();

    public static void addSingleProductDeal(SingleProductDeal singleProductDeal) {
        singleProductDeals.put(singleProductDeal.getProductId(), singleProductDeal);
        LOGGER.info("Added single product deal " + singleProductDeal.toString());
    }

    public static void addMultiProductDeal(MultiProductDeal multiProductDeal) {
        multiProductDeal.getProdIdEligibleQty().keySet().forEach(productId ->
            multiProductDeals.put(productId, multiProductDeal));
        LOGGER.info("Added multi product deal " + multiProductDeal.toString());
    }

    public static SingleProductDeal getSingleProductDeal(int productId) {
        return singleProductDeals.get(productId);
    }

    public static MultiProductDeal getMultiProductDeal(int productId) {
        return multiProductDeals.get(productId);
    }

    public static Map<Integer, SingleProductDeal> getSingleProductDeal() {
        return singleProductDeals;
    }

    public static Map<Integer, MultiProductDeal> getMultiProductDeal() {
        return multiProductDeals;
    }

    public static double calculateDiscountPrice(Map<Integer, Integer> basketInventory) {
        double totalDiscount = 0;
        Map<MultiProductDeal, Map<Integer, Double>> qualifiedOrder = new HashMap<MultiProductDeal, Map<Integer, Double>>();
        for (int productId:basketInventory.keySet()) {
            int quantity = basketInventory.get(productId);
            if (singleProductDeals.containsKey(productId)) {
                SingleProductDeal singleProductDeal = singleProductDeals.get(productId);
                double minimalQty = singleProductDeal.getMinimalQty();
                totalDiscount = totalDiscount + Math.floor(quantity / minimalQty) * minimalQty *
                        singleProductDeal.getDiscountPercent() * singleProductDeal.getProduct().getUnitPrice() ;
            }
            if (multiProductDeals.containsKey(productId)) {
                MultiProductDeal multiProductDeal = multiProductDeals.get(productId);
                Map<Integer, Double> qualifiedProductIds = qualifiedOrder.getOrDefault(multiProductDeal, new HashMap<>());
                int minQty = multiProductDeal.getProdIdEligibleQty().get(productId);
                if (quantity >= minQty) {
                    qualifiedProductIds.put(productId, Math.floor(quantity / minQty));
                    qualifiedOrder.put(multiProductDeal, qualifiedProductIds);
                }
                if (multiProductDeal.getProdIdEligibleQty().size() == qualifiedProductIds.size()) {
                    OptionalDouble optionalDouble = qualifiedOrder.get(multiProductDeal).values().stream().mapToDouble(v->v).min();
                    double qualifiedPackage = optionalDouble.isPresent() ? optionalDouble.getAsDouble() : 0.0;
                    for (int qualifiedProdId:qualifiedProductIds.keySet()) {
                        int eligibleQty = multiProductDeal.getProdIdEligibleQty().get(qualifiedProdId);
                        double discount = multiProductDeal.getProdIdDiscountPercent().get(qualifiedProdId);
                        totalDiscount = totalDiscount + qualifiedPackage * eligibleQty
                                * Inventory.getProduct(qualifiedProdId).getUnitPrice() * discount;
                    }
                }
            }
        }
        return totalDiscount;
    }
}
