package Promotion;

import java.util.Map;

public class MultiProductDeal {
    private Map<Integer, Integer> prodIdEligibleQty;
    private Map<Integer, Double> prodIdDiscountPercent;

    public MultiProductDeal(Map<Integer, Integer> prodIdEligibleQty,
                            Map<Integer, Double> prodIdDiscountPercent) {
        this.prodIdEligibleQty = prodIdEligibleQty;
        this.prodIdDiscountPercent = prodIdDiscountPercent;
    }

    public Map<Integer, Integer> getProdIdEligibleQty() {
        return this.prodIdEligibleQty;
    }

    public Map<Integer, Double> getProdIdDiscountPercent() {
        return this.prodIdDiscountPercent;
    }

    public String toString() {
        String productIdsStr = "";
        String discountStr = "";
        for (int productId:prodIdEligibleQty.keySet()) {
            productIdsStr = productIdsStr + productId + "|" + prodIdEligibleQty.get(productId) + ";";
        }
        for (int productId:prodIdDiscountPercent.keySet()) {
            discountStr = discountStr + productId + "|" + prodIdDiscountPercent.get(productId) + ";";
        }
        return "ProductId|Eligible Qty: " + productIdsStr +
                "ProductId|Discount Percent: " + discountStr;
    }
}
