package Promotion;

import Store.Inventory;
import Store.Product;

public class SingleProductDeal {
    private int productId;
    private int minimalQty;
    private double discountPercent;

    public SingleProductDeal(int productId, int minimalQty, double discountPercent) {
        this.productId = productId;
        this.minimalQty = minimalQty;
        this.discountPercent = discountPercent;
    }

    public int getProductId() {
        return this.productId;
    }

    public int getMinimalQty() {
        return this.minimalQty;
    }

    public double getDiscountPercent() {
        return this.discountPercent;
    }

    public Product getProduct() {
        return Inventory.getProduct(this.productId);
    }

    public String toString() {
        return "productId:" + productId + "|" + "minimalQty:" + minimalQty + "|" + "discountPercent:" + discountPercent;
    }
}
