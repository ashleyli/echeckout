package Store;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class Inventory {
    public static final Logger LOGGER = Logger.getLogger(Inventory.class.getName());
    private static ConcurrentHashMap<Integer, Integer> productInventory = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<Integer, Product> productIdMap = new ConcurrentHashMap<>();
    private static AtomicInteger lastProductId = new AtomicInteger(0);

    public static void createNewProduct(String description, double unitPrice) {
        int productId = lastProductId.incrementAndGet();
        Product product = new Product(productId, description, unitPrice);
        productIdMap.put(productId, product);
        LOGGER.info("Created new product: " + product.toString());
    }

    public static void amendProductDescription(int productId, String newDescription) {
        productIdMap.computeIfPresent(productId, (id, exsitingProduct) -> {
                    exsitingProduct.setDescription(newDescription);
                    LOGGER.info("Updated product description to: " + exsitingProduct.toString());
                    return exsitingProduct;
                }
        );
    }

    public static void amendProductUnitPrice(int productId, double unitPrice) {
        productIdMap.computeIfPresent(productId, (id, exsitingProduct) -> {
                    exsitingProduct.setUnitPrice(unitPrice);
                    LOGGER.info("Updated product unit price to: " + exsitingProduct.toString());
                    return exsitingProduct;
                }
        );
    }

    public static void removeProduct(int productId) {
        productInventory.computeIfPresent(productId, (id, numOfStock) -> productInventory.remove(id));
        productIdMap.computeIfPresent(productId, (id, existingProduct) -> productIdMap.remove(id));
        LOGGER.info("Removed product with id " + productId);
    }

    public static boolean addProductQty(int productId, int addedNum) {
        productInventory.computeIfPresent(productId, (id, numOfStock) -> numOfStock + addedNum);
        productInventory.putIfAbsent(productId, addedNum);
        LOGGER.info("Updated inventory for product id " + productId + " to " + productInventory.get(productId));
        return true;
    }

    public static synchronized boolean reduceProductQty(int productId, int removedNum) {
        if (checkInventory(productId, removedNum)) {
            productInventory.computeIfPresent(productId, (id, numOfStock) -> numOfStock - removedNum);
            LOGGER.info("Updated inventory for product id " + productId + " to: " + productInventory.get(productId));
            return true;
        }
        LOGGER.warning("Product with id " + productId + " do not exist or existing qty smalled than " + removedNum);
        return false;
    }

    public static boolean checkInventory(int productId, int number){
        return productInventory.get(productId) != null ? productInventory.get(productId) >= number : false;
    }

    public static Product getProduct(int productId) {
        return productIdMap.get(productId);
    }

    public static Integer getInventory(int productId) {
        return productInventory.get(productId);
    }

    public static Map<Integer, Product> getProductMap() {
        return productIdMap;
    }

    public static Map<Integer, Integer> getProductInventory() {
        return productInventory;
    }
}