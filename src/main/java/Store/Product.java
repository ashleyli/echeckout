package Store;

public class Product {
    private int productId;
    private String description;
    private double unitPrice;

    public Product(int productId, String description, double unitPrice) {
        this.description = description;
        this.unitPrice = unitPrice;
        this.productId = productId;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public double getUnitPrice() {
        return this.unitPrice;
    }

    public int getProductId() {
        return this.productId;
    }

    public String toString() {
        return productId + "|" + description + "|" + unitPrice;
    }
}