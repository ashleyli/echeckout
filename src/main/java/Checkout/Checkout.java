package Checkout;

import Promotion.Promotion;
import Store.Inventory;
import Store.Product;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class Checkout {
    public static final Logger LOGGER = Logger.getLogger(Checkout.class.getName());
    private Map<Integer, Integer> basketInventory = new HashMap<>();

    public void addProduct(int productId, int addedNum) {
        if (Inventory.reduceProductQty(productId, addedNum)) {
            basketInventory.computeIfPresent(productId, (id, numOfStock) -> numOfStock + addedNum);
            basketInventory.putIfAbsent(productId, addedNum);
            LOGGER.info("Added " + addedNum + " product " + productId + "into shopping basket." );
        }
    }

    public void amendQty(int productId, int amendedQty) {
        int existingQty = basketInventory.get(productId);
        boolean success = amendedQty > existingQty ?
                Inventory.reduceProductQty(productId, amendedQty - existingQty) :
                Inventory.addProductQty(productId, existingQty - amendedQty);
        if (success) {
            basketInventory.computeIfPresent(productId, (id, numOfStock) -> amendedQty);
        }
    }

    public void removeProduct(int productId) {
        basketInventory.computeIfPresent(productId, (id, numOfStock) -> {
            Inventory.addProductQty(productId, numOfStock);
            return basketInventory.remove(id);
        });
    }

    public double getTotalPrice(){
        double totalPrice = 0;
        double discountPrice = Promotion.calculateDiscountPrice(basketInventory);
        for (int productId:basketInventory.keySet()) {
            Product product = Inventory.getProduct(productId);
            totalPrice = totalPrice + product.getUnitPrice() * basketInventory.get(productId);
        };
        return totalPrice - discountPrice;
    }

    public Map<Integer, Integer> getShoppingBasket() {
        return this.basketInventory;
    }
}